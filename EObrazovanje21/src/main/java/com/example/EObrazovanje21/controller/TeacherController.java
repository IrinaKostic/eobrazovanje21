package com.example.EObrazovanje21.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestBody;
import com.example.EObrazovanje21.DTO.TeacherDTO;
import com.example.EObrazovanje21.model.Teacher;
import com.example.EObrazovanje21.service.TeacherService;




@RestController
@RequestMapping(value = "api/teachers")
public class TeacherController {
	
	@Autowired
	private TeacherService teacherService;
	
	@RequestMapping(value = "/all", method = RequestMethod.GET)
		public ResponseEntity<List<TeacherDTO>>getAllTeachers(){
			List<Teacher> teachers = new ArrayList<>();
			//convert teachers to DTOs
			List<TeacherDTO> teachersDTO = new ArrayList<>();
			for (Teacher t : teachers) {
				teachersDTO.add(new TeacherDTO(t));
			}
			return new ResponseEntity<>(teachersDTO, HttpStatus.OK);
			
		}
	
	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<TeacherDTO>> getTeachersPage(Pageable page) {

		Page<Teacher> teachers = teacherService.findAll(page);
		
		//convert teachers to DTOs
		List<TeacherDTO> teachersDTO = new ArrayList<>();
		for (Teacher t : teachers) {
			teachersDTO.add(new TeacherDTO(t));
		}
		return new ResponseEntity<>(teachersDTO, HttpStatus.OK);
	}
	@RequestMapping(value="/{id}", method=RequestMethod.GET)
	public ResponseEntity<TeacherDTO> getTeacher(@PathVariable Long id){
		Teacher teacher = teacherService.findOne(id);
		if(teacher == null){
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		
		return new ResponseEntity<>(new TeacherDTO(teacher), HttpStatus.OK);
	}


	@RequestMapping(method=RequestMethod.POST, consumes="application/json")
	public ResponseEntity<TeacherDTO> saveTeacher(@RequestBody TeacherDTO teacherDTO){		
		Teacher teacher = new Teacher();
		
		teacher.setFirstName(teacherDTO.getFirstName());
		teacher.setLastName(teacherDTO.getLastName());
		
		
		
		teacher = teacherService.save(teacher);
		return new ResponseEntity<>(new TeacherDTO(teacher), HttpStatus.CREATED);	
		}
	@RequestMapping(method=RequestMethod.PUT, consumes="application/json")
	public ResponseEntity<TeacherDTO> updateTeacher(@RequestBody TeacherDTO teacherDTO){
		//nastavnik mora postojati
		Teacher teacher = teacherService.findOne(teacherDTO.getId()); 
		if (teacher == null) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		
		
		teacher.setFirstName(teacherDTO.getFirstName());
		teacher.setLastName(teacherDTO.getLastName());
		
		teacher = teacherService.save(teacher);
		return new ResponseEntity<>(new TeacherDTO(teacher), HttpStatus.OK);	
	}

	@RequestMapping(value="/{id}", method=RequestMethod.DELETE)
	public ResponseEntity<Void> deleteTeacher(@PathVariable Long id){
		Teacher teacher = teacherService.findOne(id);
		if (teacher != null){
			teacherService.delete(id);
			return new ResponseEntity<>(HttpStatus.OK);
		} else {		
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	
	
 }



