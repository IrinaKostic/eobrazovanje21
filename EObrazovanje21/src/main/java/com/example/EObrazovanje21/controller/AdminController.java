package com.example.EObrazovanje21.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import com.example.EObrazovanje21.DTO.AdminDTO;

import com.example.EObrazovanje21.model.Admin;

import com.example.EObrazovanje21.service.AdminService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

@RestController
@RequestMapping(value = "api/admins")
public class AdminController {
	
	@Autowired
	private AdminService adminService;
	
	@RequestMapping(value = "/all",method = RequestMethod.GET)
	public ResponseEntity<List<AdminDTO>>getAllAdmins(){
		List<Admin> admins = new ArrayList<>();
		//convert admins to DTOs
		List<AdminDTO> adminsDTO = new ArrayList<>();
		for (Admin a : admins) {
			adminsDTO.add(new AdminDTO(a));
		}
		return new ResponseEntity<>(adminsDTO, HttpStatus.OK);
		
	}
	
	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<AdminDTO>> getAdminsPage(Pageable page) {

		Page<Admin> admins = adminService.findAll(page);
		
		//convert admins to DTOs
		List<AdminDTO> adminsDTO = new ArrayList<>();
		for (Admin a : admins) {
			adminsDTO.add(new AdminDTO(a));
		}
		return new ResponseEntity<>(adminsDTO, HttpStatus.OK);
	}
	
	@RequestMapping(value="/{id}", method=RequestMethod.GET)
	public ResponseEntity<AdminDTO> getAdmin(@PathVariable Long id){
		Admin admin = adminService.findOne(id);
		if(admin == null){
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		
		return new ResponseEntity<>(new AdminDTO(admin), HttpStatus.OK);
	}
	
	@RequestMapping(method=RequestMethod.POST, consumes="application/json")
	public ResponseEntity<AdminDTO> saveAdmin(@RequestBody AdminDTO adminDTO){		
		Admin admin = new Admin();
		
		admin.setFirstName(adminDTO.getFirstName());
		admin.setLastName(adminDTO.getLastName());
		
		admin = adminService.save(admin);
		return new ResponseEntity<>(new AdminDTO(admin), HttpStatus.CREATED);	
		}
	
	@RequestMapping(method=RequestMethod.PUT, consumes="application/json")
	public ResponseEntity<AdminDTO> updateAdmin(@RequestBody AdminDTO adminDTO){
		//admin mora postojati
		Admin admin = adminService.findOne(adminDTO.getId()); 
		if (admin == null) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		
		
		admin.setFirstName(adminDTO.getFirstName());
		admin.setLastName(adminDTO.getLastName());
		
		admin = adminService.save(admin);
		return new ResponseEntity<>(new AdminDTO(admin), HttpStatus.OK);	
	}
	
	@RequestMapping(value="/{id}", method=RequestMethod.DELETE)
	public ResponseEntity<Void> deleteAdmin(@PathVariable Long id){
		Admin admin = adminService.findOne(id);
		if (admin != null){
			adminService.remove(id);
			return new ResponseEntity<>(HttpStatus.OK);
		} else {		
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
		
}


