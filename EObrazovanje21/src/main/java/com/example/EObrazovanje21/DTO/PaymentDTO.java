package com.example.EObrazovanje21.DTO;

import java.sql.Date;

import com.example.EObrazovanje21.model.Payment;



public class PaymentDTO {
	

	private Long id;
	private Double amount;
	private Date paymentDate;
	private String note;
	private AccountDTO account;
	
	
	public PaymentDTO(Long id, Double amount, Date paymentDate, String note, AccountDTO account) {
		super();
		this.id = id;
		this.amount = amount;
		this.paymentDate = paymentDate;
		this.note = note;
		this.account = account;
	}
	
	public PaymentDTO (Payment payment) {
		this(payment.getId(), 
			 payment.getAmount(),
			 payment.getPaymentDate(),
			 payment.getNote(),
			 new AccountDTO (payment.getAccount()));
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public Date getPaymentDate() {
		return paymentDate;
	}

	public void setPaymentDate(Date paymentDate) {
		this.paymentDate = paymentDate;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public AccountDTO getAccount() {
		return account;
	}

	public void setAccount(AccountDTO account) {
		this.account = account;
	}

	
}
