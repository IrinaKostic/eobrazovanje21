package com.example.EObrazovanje21.DTO;

import com.example.EObrazovanje21.model.Admin;

public class AdminDTO {
	
	private Long id;
	private String firstName;
	private String lastName;
	private UserDTO user;
	
	public AdminDTO() {
		
	}

	public AdminDTO(Long id, String firstName, String lastName, UserDTO user) {
		super();
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.user = user;
	}
	public AdminDTO(Admin admin) {
		this(admin.getId(),
				admin.getFirstName(),
				admin.getLastName(),
				new UserDTO(admin.getUser()));
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public UserDTO getUser() {
		return user;
	}

	public void setUser(UserDTO user) {
		this.user = user;
	}
	
	

}
