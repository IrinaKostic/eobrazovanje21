package com.example.EObrazovanje21.DTO;

import java.util.ArrayList
;

import com.example.EObrazovanje21.model.Teacher;


public class TeacherDTO {
	
	private Long id;
	private String firstName;
	private String lastName;
	private ArrayList<TeachingDTO> teaching;
	private UserDTO user;
	
	public TeacherDTO() {
		
	}
	public TeacherDTO(Long id, String firstName, String lastName, ArrayList<TeachingDTO> teaching, UserDTO user) {
		super();
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.teaching = teaching;
		this.user = user;
	}
	public TeacherDTO(Teacher teacher) {
		this(teacher.getId(),
				teacher.getFirstName(),
				teacher.getLastName(),	
			 new ArrayList<TeachingDTO>(),
			 new UserDTO(teacher.getUser()));
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public ArrayList<TeachingDTO> getTeaching() {
		return teaching;
	}
	public void setTeaching(ArrayList<TeachingDTO> teaching) {
		this.teaching = teaching;
	}
	public UserDTO getUser() {
		return user;
	}
	public void setUser(UserDTO user) {
		this.user = user;
	}
	
}
