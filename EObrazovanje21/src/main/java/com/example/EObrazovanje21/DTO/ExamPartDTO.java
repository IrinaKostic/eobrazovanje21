package com.example.EObrazovanje21.DTO;

import java.util.Date;

import com.example.EObrazovanje21.model.ExamPart;

public class ExamPartDTO {
	
	private Long id;
	private String location;
	private Date date;
	private int points;
	
	public ExamPartDTO() {
		
	}

	public ExamPartDTO(Long id, String location, Date date, int points) {
		super();
		this.id = id;
		this.location = location;
		this.date = date;
		this.points = points;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public int getPoints() {
		return points;
	}

	public void setPoints(int points) {
		this.points = points;
	}

	
}
