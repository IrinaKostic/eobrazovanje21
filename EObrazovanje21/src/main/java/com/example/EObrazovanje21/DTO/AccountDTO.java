package com.example.EObrazovanje21.DTO;

import java.util.ArrayList;


import com.example.EObrazovanje21.model.Account;

public class AccountDTO {
	
	private Long id;
	private String name;
	private Double amount;
	private StudentDTO student;
	private ArrayList<PaymentDTO> payments ;
	


	public AccountDTO(Long id, String name, Double amount, StudentDTO student, ArrayList<PaymentDTO> payments) {
		super();
		this.id = id;
		this.name = name;
		this.amount = amount;
		this.student = student;
		this.payments = payments;
	}



	public AccountDTO(Account account) {
	        this(account.getId(),
	        	account.getName(), 
	        	account.getAmount(),
	        	new StudentDTO(account.getStudent()),
	        	new ArrayList<PaymentDTO>()); 

	    }



	public Long getId() {
		return id;
	}



	public void setId(Long id) {
		this.id = id;
	}



	public String getName() {
		return name;
	}



	public void setName(String name) {
		this.name = name;
	}



	public Double getAmount() {
		return amount;
	}



	public void setAmount(Double amount) {
		this.amount = amount;
	}



	public StudentDTO getStudent() {
		return student;
	}



	public void setStudent(StudentDTO student) {
		this.student = student;
	}



	public ArrayList<PaymentDTO> getPayments() {
		return payments;
	}



	public void setPayments(ArrayList<PaymentDTO> payments) {
		this.payments = payments;
	}
	
	 
}
