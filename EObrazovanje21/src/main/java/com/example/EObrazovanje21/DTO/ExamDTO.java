package com.example.EObrazovanje21.DTO;


public class ExamDTO {
	
	private Long id;
	private int points;
	private int grade;
	
	public ExamDTO() {
		
	}

	public ExamDTO(Long id, int points, int grade) {
		super();
		this.id = id;
		this.points = points;
		this.grade = grade;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getPoints() {
		return points;
	}

	public void setPoints(int points) {
		this.points = points;
	}

	public int getGrade() {
		return grade;
	}

	public void setGrade(int grade) {
		this.grade = grade;
	}

	
	
	

}
