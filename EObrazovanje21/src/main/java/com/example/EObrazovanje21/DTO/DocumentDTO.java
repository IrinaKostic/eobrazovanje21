package com.example.EObrazovanje21.DTO;


import com.example.EObrazovanje21.model.Document;
import com.example.EObrazovanje21.model.DocumentType;

public class DocumentDTO {
	
	private Long id;
	private String title;
	private String url;
	private DocumentType documentType;
	private StudentDTO student;
	
	
	public DocumentDTO(Long id, String title, String url, DocumentType documentType, StudentDTO student) {
		super();
		this.id = id;
		this.title = title;
		this.url = url;
		this.documentType = documentType;
		this.student = student;
	}
	
	public DocumentDTO(Document document) {
		this(document.getId(),
			 document.getTitle(),
			 document.getUrl(),
			 document.getDocumentType(), 
			 new StudentDTO(document.getStudent()));
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public DocumentType getDocumentType() {
		return documentType;
	}

	public void setDocumentType(DocumentType documentType) {
		this.documentType = documentType;
	}

	public StudentDTO getStudent() {
		return student;
	}

	public void setStudent(StudentDTO student) {
		this.student = student;
	}

	
	
}
