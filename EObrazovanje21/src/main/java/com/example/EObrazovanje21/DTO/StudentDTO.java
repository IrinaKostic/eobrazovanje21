package com.example.EObrazovanje21.DTO;

import java.util.ArrayList;


import com.example.EObrazovanje21.model.Student;

public class StudentDTO {
	
	private Long id;
	private String cardNumber;
	private String firstName;
	private String lastName;
	private int umnc;
	private ArrayList<DocumentDTO> document;
	private EnrollmentDTO enrollment;
	private AccountDTO account;
	private UserDTO user;
	
	
	
	
	public StudentDTO(Long id, String cardNumber, String firstName, String lastName, int umnc,
			ArrayList<DocumentDTO> document, EnrollmentDTO enrollment, AccountDTO account, UserDTO user) {
		super();
		this.id = id;
		this.cardNumber = cardNumber;
		this.firstName = firstName;
		this.lastName = lastName;
		this.umnc = umnc;
		this.document = document;
		this.enrollment = enrollment;
		this.account = account;
		this.user = user;
	}


	public StudentDTO(Student student) {
		this(student.getId(),
			 student.getCardNumber(),
			 student.getFirstName(),
			 student.getLastName(),
			 student.getUmnc(),
			 new ArrayList<DocumentDTO>(),
			 new EnrollmentDTO(student.getEnrollment()),
			 new AccountDTO(student.getAccount()),
			 new UserDTO(student.getUser()));
	}
	
	
	


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public String getCardNumber() {
		return cardNumber;
	}


	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}


	public String getFirstName() {
		return firstName;
	}


	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}


	public String getLastName() {
		return lastName;
	}


	public void setLastName(String lastName) {
		this.lastName = lastName;
	}


	public int getUmnc() {
		return umnc;
	}


	public void setUmnc(int umnc) {
		this.umnc = umnc;
	}


	public ArrayList<DocumentDTO> getDocument() {
		return document;
	}


	public void setDocument(ArrayList<DocumentDTO> document) {
		this.document = document;
	}


	public EnrollmentDTO getEnrollment() {
		return enrollment;
	}


	public void setEnrollment(EnrollmentDTO enrollment) {
		this.enrollment = enrollment;
	}


	public AccountDTO getAccount() {
		return account;
	}


	public void setAccount(AccountDTO account) {
		this.account = account;
	}


	public UserDTO getUser() {
		return user;
	}


	public void setUser(UserDTO user) {
		this.user = user;
	}
	
	
	
	
	

}
