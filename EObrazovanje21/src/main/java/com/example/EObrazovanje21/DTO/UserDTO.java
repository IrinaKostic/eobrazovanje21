package com.example.EObrazovanje21.DTO;

import java.util.ArrayList;

import com.example.EObrazovanje21.model.Admin;
import com.example.EObrazovanje21.model.Student;
import com.example.EObrazovanje21.model.Teacher;
import com.example.EObrazovanje21.model.User;
import com.example.EObrazovanje21.model.UserTypeE;

public class UserDTO {
	
	private Long id;
	private String username;
	private String password;
	private String email;
	private UserTypeE userType;
	private ArrayList<AdminDTO>admin;
	private ArrayList<TeacherDTO>teacher;
	private ArrayList<StudentDTO>student;
	
	public UserDTO() {
		
	}
	public UserDTO(Long id, String username, String password, String email, UserTypeE userType,
			ArrayList<AdminDTO> admin, ArrayList<TeacherDTO> teacher, ArrayList<StudentDTO> student) {
		super();
		this.id = id;
		this.username = username;
		this.password = password;
		this.email = email;
		this.userType = userType;
		this.admin = admin;
		this.teacher = teacher;
		this.student = student;
	}
	public UserDTO(User user) {
		this(user.getId(),
				user.getUsername(),
				user.getPassword(),
				user.getEmail(),
				user.getUserTypeE(),
			 new ArrayList<AdminDTO>(),
			 new ArrayList<TeacherDTO>(),
			 new ArrayList<StudentDTO>()
			);
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public UserTypeE getUserType() {
		return userType;
	}
	public void setUserType(UserTypeE userType) {
		this.userType = userType;
	}
	public ArrayList<AdminDTO> getAdmin() {
		return admin;
	}
	public void setAdmin(ArrayList<AdminDTO> admin) {
		this.admin = admin;
	}
	public ArrayList<TeacherDTO> getTeacher() {
		return teacher;
	}
	public void setTeacher(ArrayList<TeacherDTO> teacher) {
		this.teacher = teacher;
	}
	public ArrayList<StudentDTO> getStudent() {
		return student;
	}
	public void setStudent(ArrayList<StudentDTO> student) {
		this.student = student;
	}
	
}
