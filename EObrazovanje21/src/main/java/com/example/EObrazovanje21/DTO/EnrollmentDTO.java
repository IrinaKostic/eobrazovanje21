package com.example.EObrazovanje21.DTO;

import java.util.Date;
import java.util.Set;

import com.example.EObrazovanje21.model.CourseInstance;
import com.example.EObrazovanje21.model.CourseSpecification;
import com.example.EObrazovanje21.model.Enrollment;
import com.example.EObrazovanje21.model.Exam;
import com.example.EObrazovanje21.model.Student;

public class EnrollmentDTO {

	private Long id;
	private Date startDate;
	private Date endDate;
	private Student student;
	private CourseSpecification courseSpecification;
	private Exam exam;
	
	
	
	public EnrollmentDTO() {
		
	}
	public EnrollmentDTO(Enrollment enrollment) {
		this(enrollment.getId(),
				enrollment.getStartDate(),
				enrollment.getEndDate(),
				enrollment.getStudent(),
				enrollment.getCourseSpecification(),
				enrollment.getExam());
	}

	public EnrollmentDTO(Long id, Date startDate, Date endDate, Student student, CourseSpecification courseSpecification,
			Exam exam) {
		super();
		this.id = id;
		this.startDate = startDate;
		this.endDate = endDate;
		this.student = student;
		this.courseSpecification = courseSpecification;
		this.exam = exam;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public Student getStudent() {
		return student;
	}

	public void setStudent(Student student) {
		this.student = student;
	}
	public CourseSpecification getCourseSpecification() {
		return courseSpecification;
	}
	public void setCourseSpecification(CourseSpecification courseSpecification) {
		this.courseSpecification = courseSpecification;
	}
	public Exam getExam() {
		return exam;
	}

	public void setExam(Exam exam) {
		this.exam = exam;
	}
	
	
}
