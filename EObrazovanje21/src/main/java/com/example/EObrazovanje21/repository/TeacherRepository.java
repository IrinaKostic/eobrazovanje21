package com.example.EObrazovanje21.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.EObrazovanje21.model.Teacher;

public interface TeacherRepository extends JpaRepository<Teacher, Long> {

}
