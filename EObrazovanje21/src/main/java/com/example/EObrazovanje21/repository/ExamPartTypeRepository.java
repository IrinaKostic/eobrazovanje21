package com.example.EObrazovanje21.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.EObrazovanje21.model.ExamPartType;

public interface ExamPartTypeRepository extends JpaRepository<ExamPartType, Long> {

}
