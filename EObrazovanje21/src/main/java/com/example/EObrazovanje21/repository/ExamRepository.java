package com.example.EObrazovanje21.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.EObrazovanje21.model.Exam;

public interface ExamRepository extends JpaRepository<Exam, Long> {

}
