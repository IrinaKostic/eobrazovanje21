package com.example.EObrazovanje21.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.EObrazovanje21.model.CourseInstance;

public interface CourseInstanceRepository extends JpaRepository<CourseInstance, Long> {

}
