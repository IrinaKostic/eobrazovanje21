package com.example.EObrazovanje21.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.EObrazovanje21.model.ExamPart;

public interface ExamPartRepository extends JpaRepository<ExamPart, Long> {

}
