package com.example.EObrazovanje21.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.EObrazovanje21.model.Payment;

public interface PaymentRepository extends JpaRepository<Payment, Long> {

}
