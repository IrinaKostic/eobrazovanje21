package com.example.EObrazovanje21.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.EObrazovanje21.model.Document;

public interface DocumentRepository extends JpaRepository<Document, Long> {

}
