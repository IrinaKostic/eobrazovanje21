package com.example.EObrazovanje21.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.EObrazovanje21.model.Teaching;

public interface TeachingRepository extends JpaRepository<Teaching, Long> {

}
