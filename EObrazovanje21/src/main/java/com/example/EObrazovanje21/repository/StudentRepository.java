package com.example.EObrazovanje21.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.EObrazovanje21.model.Student;

public interface StudentRepository extends JpaRepository<Student, Long> {

	Student findOneByCardNumber(String cardNumber);

	List<Student> findAllByUmnc(int umnc);

}
