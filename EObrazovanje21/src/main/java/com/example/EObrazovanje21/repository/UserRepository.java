package com.example.EObrazovanje21.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.EObrazovanje21.model.User;

public interface UserRepository extends JpaRepository<User, Long> {

}
