package com.example.EObrazovanje21.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.EObrazovanje21.model.Admin;

public interface AdminRepository extends JpaRepository<Admin, Long> {

}
