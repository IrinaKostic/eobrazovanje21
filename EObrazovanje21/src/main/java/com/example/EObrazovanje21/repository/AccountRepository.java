package com.example.EObrazovanje21.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.EObrazovanje21.model.Account;

public interface AccountRepository extends JpaRepository<Account, Long> {

	

}
