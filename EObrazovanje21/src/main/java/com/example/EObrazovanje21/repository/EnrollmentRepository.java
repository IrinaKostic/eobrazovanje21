package com.example.EObrazovanje21.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.EObrazovanje21.model.Enrollment;

public interface EnrollmentRepository extends JpaRepository<Enrollment, Long> {

}
