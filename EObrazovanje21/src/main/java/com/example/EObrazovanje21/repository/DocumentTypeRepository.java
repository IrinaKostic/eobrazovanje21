package com.example.EObrazovanje21.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.EObrazovanje21.model.DocumentType;

public interface DocumentTypeRepository extends JpaRepository<DocumentType, Long> {

}
