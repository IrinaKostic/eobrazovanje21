package com.example.EObrazovanje21.model;

public enum TeachingTypeE {
	
	lecture,
	exercises,
	laboratoryExercises

}
