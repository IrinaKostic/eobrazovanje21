package com.example.EObrazovanje21.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;



@Entity
@Table(name = "user")
public class User {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "user_id", unique = true, nullable = false)
	private Long id;
	
	@Column(name = "username", unique = false, nullable = false)
	private String username;
	
	@Column(name = "password", unique = false, nullable = false)
	private String password;
	
	@Column(name = "email", unique = false, nullable = false)
	private String email;
	
	@Column(name = "userTypeE", unique = false, nullable = false)
	private UserTypeE userTypeE;
	
	@OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "admin", referencedColumnName = "admin_id")
	private Admin admin;
	
	@OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "teacher", referencedColumnName = "teacher_id")
	private Teacher teacher;
	
	@OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "student", referencedColumnName = "student_id")
	private Student student;

	public User(Long id, String username, String password, String email, UserTypeE userTypeE, Admin admin,
			Teacher teacher, Student student) {
		super();
		this.id = id;
		this.username = username;
		this.password = password;
		this.email = email;
		this.userTypeE = userTypeE;
		this.admin = admin;
		this.teacher = teacher;
		this.student = student;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public UserTypeE getUserTypeE() {
		return userTypeE;
	}

	public void setUserTypeE(UserTypeE userTypeE) {
		this.userTypeE = userTypeE;
	}

	public Admin getAdmin() {
		return admin;
	}

	public void setAdmin(Admin admin) {
		this.admin = admin;
	}

	public Teacher getTeacher() {
		return teacher;
	}

	public void setTeacher(Teacher teacher) {
		this.teacher = teacher;
	}

	public Student getStudent() {
		return student;
	}

	public void setStudent(Student student) {
		this.student = student;
	}
	
	

}
