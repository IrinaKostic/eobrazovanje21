package com.example.EObrazovanje21.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "examPartType")
public class ExamPartType {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "exam_part_type_id", unique = true, nullable = false)
	private Long id;
	
	@Column(name = "name", unique = false, nullable = false)
	private String name;
	
	@Column(name = "code", unique = false, nullable = false)
	private String code;
	
	@Column(name = "exam_part_type", unique = false, nullable = false)
	private ExamPartTypeE examPartTypeE;
	
	@OneToMany(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY, mappedBy = "examPartType")
	private Set<ExamPart> examPart = new HashSet<ExamPart>();

	public ExamPartType(Long id, String name, String code, ExamPartTypeE examPartTypeE, Set<ExamPart> examPart) {
		super();
		this.id = id;
		this.name = name;
		this.code = code;
		this.examPartTypeE = examPartTypeE;
		this.examPart = examPart;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public ExamPartTypeE getExamPartTypeE() {
		return examPartTypeE;
	}

	public void setExamPartTypeE(ExamPartTypeE examPartTypeE) {
		this.examPartTypeE = examPartTypeE;
	}

	public Set<ExamPart> getExamPart() {
		return examPart;
	}

	public void setExamPart(Set<ExamPart> examPart) {
		this.examPart = examPart;
	}
	
	

}
