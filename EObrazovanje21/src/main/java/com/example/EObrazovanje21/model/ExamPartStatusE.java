package com.example.EObrazovanje21.model;

public enum ExamPartStatusE {
	
	passed,
	failed,
	cancelled,
	NA

}
