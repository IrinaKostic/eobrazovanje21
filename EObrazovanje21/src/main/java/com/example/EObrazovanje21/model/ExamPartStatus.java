package com.example.EObrazovanje21.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "examPartStatus")
public class ExamPartStatus {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "exam_part_status_id", unique = true, nullable = false)
	private Long id;
	
	@Column(name = "name", unique = false, nullable = false)
	private String name;
	
	@Column(name = "code", unique = false, nullable = false)
	private String code;
	
	@Column(name = "exam_part_status", unique = false, nullable = false)
	private ExamPartStatusE examPartStatusE;
	
	@OneToMany(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY, mappedBy = "examPartStatus")
	private Set<ExamPart> examPart = new HashSet<ExamPart>();

	public ExamPartStatus(Long id, String name, String code, ExamPartStatusE examPartStatusE, Set<ExamPart> examPart) {
		super();
		this.id = id;
		this.name = name;
		this.code = code;
		this.examPartStatusE = examPartStatusE;
		this.examPart = examPart;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public ExamPartStatusE getExamPartStatusE() {
		return examPartStatusE;
	}

	public void setExamPartStatusE(ExamPartStatusE examPartStatusE) {
		this.examPartStatusE = examPartStatusE;
	}

	public Set<ExamPart> getExamPart() {
		return examPart;
	}

	public void setExamPart(Set<ExamPart> examPart) {
		this.examPart = examPart;
	}
	
	

}
