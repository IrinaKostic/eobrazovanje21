package com.example.EObrazovanje21.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "teachingType")
public class TeachingType {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "teaching_type_id", unique = true, nullable = false)
	private Long id;
	
	@Column(name = "name", unique = false, nullable = false)
	private String name;
	
	@Column(name = "code", unique = false, nullable = false)
	private String code;
	
	@Column(name = "teaching_Type", unique = false, nullable = false)
	private TeachingTypeE teachingTypeE;
	
	@OneToMany(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY, mappedBy = "teachingType")
	private Set<Teaching> teachingType = new HashSet<Teaching>();

	public TeachingType(Long id, String name, String code, TeachingTypeE teachingTypeE, Set<Teaching> teachingType) {
		super();
		this.id = id;
		this.name = name;
		this.code = code;
		this.teachingTypeE = teachingTypeE;
		this.teachingType = teachingType;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public TeachingTypeE getTeachingTypeE() {
		return teachingTypeE;
	}

	public void setTeachingTypeE(TeachingTypeE teachingTypeE) {
		this.teachingTypeE = teachingTypeE;
	}

	public Set<Teaching> getTeachingType() {
		return teachingType;
	}

	public void setTeachingType(Set<Teaching> teachingType) {
		this.teachingType = teachingType;
	}
	
	


}
