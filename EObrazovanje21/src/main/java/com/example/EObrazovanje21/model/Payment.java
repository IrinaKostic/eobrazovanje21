package com.example.EObrazovanje21.model;

import java.sql.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "payment")
public class Payment {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "payment_id", unique = true, nullable = false)
	private Long id;
	
	@Column(name = "amount", unique = false, nullable = false)
	private Double amount;
	
	
	@Column(name = "paymentDate", unique = false, nullable = false)
	private Date paymentDate;
	
	@Column(name = "note", unique = false, nullable = false)
	private String note;

	@ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "account", referencedColumnName ="account_id", nullable = true)
	private Account account;

	public Payment(Long id, Double amount, Date paymentDate, String note, Account account) {
		super();
		this.id = id;
		this.amount = amount;
		this.paymentDate = paymentDate;
		this.note = note;
		this.account = account;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public Date getPaymentDate() {
		return paymentDate;
	}

	public void setPaymentDate(Date paymentDate) {
		this.paymentDate = paymentDate;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public Account getAccount() {
		return account;
	}

	public void setAccount(Account account) {
		this.account = account;
	}
	
	
}
