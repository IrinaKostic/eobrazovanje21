package com.example.EObrazovanje21.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name = "document")
public class Document {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "document_id", unique = true, nullable = false)
	private Long id;
	
	@Column(name = "title", unique = false, nullable = false)
	private String title;
	
	@Column(name = "url", unique = false, nullable = false)
	private String url;
	
	@ManyToOne
	@JoinColumn(name="document_type", referencedColumnName="document_type_id", nullable=true)
	private DocumentType documentType;
	
	@ManyToOne
	@JoinColumn(name="student", referencedColumnName="student_id", nullable=true)
	private Student student;

	public Document(Long id, String title, String url, DocumentType documentType, Student student) {
		super();
		this.id = id;
		this.title = title;
		this.url = url;
		this.documentType = documentType;
		this.student = student;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public DocumentType getDocumentType() {
		return documentType;
	}

	public void setDocumentType(DocumentType documentType) {
		this.documentType = documentType;
	}

	public Student getStudent() {
		return student;
	}

	public void setStudent(Student student) {
		this.student = student;
	}


	
	

}
