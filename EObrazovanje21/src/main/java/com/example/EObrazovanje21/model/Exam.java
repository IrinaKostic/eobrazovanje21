package com.example.EObrazovanje21.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "exam")
public class Exam {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "exam_id", unique = true, nullable = false)
	private Long id;
	
	@Column(name = "points", unique = false, nullable = false)
	private int points;
	
	@Column(name = "grade", unique = false, nullable = false)
	private int grade;
	
	@OneToMany(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY, mappedBy = "exam")
	private Set<Enrollment> enrollment = new HashSet<Enrollment>();
	
	@OneToMany(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY, mappedBy = "exam")
	private Set<ExamPart> examPart = new HashSet<ExamPart>();

	public Exam(Long id, int points, int grade, Set<Enrollment> enrollment, Set<ExamPart> examPart) {
		super();
		this.id = id;
		this.points = points;
		this.grade = grade;
		this.enrollment = enrollment;
		this.examPart = examPart;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getPoints() {
		return points;
	}

	public void setPoints(int points) {
		this.points = points;
	}

	public int getGrade() {
		return grade;
	}

	public void setGrade(int grade) {
		this.grade = grade;
	}

	public Set<Enrollment> getEnrollment() {
		return enrollment;
	}

	public void setEnrollment(Set<Enrollment> enrollment) {
		this.enrollment = enrollment;
	}

	public Set<ExamPart> getExamPart() {
		return examPart;
	}

	public void setExamPart(Set<ExamPart> examPart) {
		this.examPart = examPart;
	}
	
	

}
