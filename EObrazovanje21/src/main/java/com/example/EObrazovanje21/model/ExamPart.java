package com.example.EObrazovanje21.model;

import java.sql.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "examPart")
public class ExamPart {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "exam_part_id", unique = true, nullable = false)
	private Long id;
	
	@Column(name = "location", unique = false, nullable = false)
	private String location;
	
	@Column(name = "date", unique = false, nullable = false)
	private Date date;
	
	@Column(name = "points", unique = false, nullable = false)
	private int points;
	
	@ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "exam", referencedColumnName ="exam_id", nullable = true)
	private Exam exam;
	
	@ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "examPartType", referencedColumnName ="exam_part_type_id", nullable = true)
	private ExamPartType examPartType;
	
	@ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "examPartStatus", referencedColumnName ="exam_part_status_id", nullable = true)
	private ExamPartStatus examPartStatus;

	public ExamPart(Long id, String location, Date date, int points, Exam exam, ExamPartType examPartType,
			ExamPartStatus examPartStatus) {
		super();
		this.id = id;
		this.location = location;
		this.date = date;
		this.points = points;
		this.exam = exam;
		this.examPartType = examPartType;
		this.examPartStatus = examPartStatus;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public int getPoints() {
		return points;
	}

	public void setPoints(int points) {
		this.points = points;
	}

	public Exam getExam() {
		return exam;
	}

	public void setExam(Exam exam) {
		this.exam = exam;
	}

	public ExamPartType getExamPartType() {
		return examPartType;
	}

	public void setExamPartType(ExamPartType examPartType) {
		this.examPartType = examPartType;
	}

	public ExamPartStatus getExamPartStatus() {
		return examPartStatus;
	}

	public void setExamPartStatus(ExamPartStatus examPartStatus) {
		this.examPartStatus = examPartStatus;
	}
	
	

}
