package com.example.EObrazovanje21.model;

import java.sql.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;



@Entity
@Table(name = "course_instance")
public class CourseInstance {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "course_instance_id", unique = true, nullable = false)
	private Long id;
	
	@Column(name = "start_date", unique = false, nullable = false)
	private Date startDate;
	
	@Column(name = "end_date", unique = false, nullable = false)
	private Date endDate;
	
	@OneToMany(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY, mappedBy = "course_instance")
	private Set<CourseSpecification> courseSpecification = new HashSet<CourseSpecification>();
	
	@OneToMany(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY, mappedBy = "course_instance")
	private Set<Enrollment> enrollment = new HashSet<Enrollment>();
	
	@OneToMany(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY, mappedBy = "course_instance")
	private Set<Teaching> teaching = new HashSet<Teaching>();

	public CourseInstance(Long id, Date startDate, Date endDate, Set<CourseSpecification> courseSpecification,
			Set<Enrollment> enrollment, Set<Teaching> teaching) {
		super();
		this.id = id;
		this.startDate = startDate;
		this.endDate = endDate;
		this.courseSpecification = courseSpecification;
		this.enrollment = enrollment;
		this.teaching = teaching;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public Set<CourseSpecification> getCourseSpecification() {
		return courseSpecification;
	}

	public void setCourseSpecification(Set<CourseSpecification> courseSpecification) {
		this.courseSpecification = courseSpecification;
	}

	public Set<Enrollment> getEnrollment() {
		return enrollment;
	}

	public void setEnrollment(Set<Enrollment> enrollment) {
		this.enrollment = enrollment;
	}

	public Set<Teaching> getTeaching() {
		return teaching;
	}

	public void setTeaching(Set<Teaching> teaching) {
		this.teaching = teaching;
	}
	
	

	

}
