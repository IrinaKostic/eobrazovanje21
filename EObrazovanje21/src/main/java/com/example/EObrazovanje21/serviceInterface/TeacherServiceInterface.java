package com.example.EObrazovanje21.serviceInterface;

import java.util.List;

import com.example.EObrazovanje21.model.Teacher;

public interface TeacherServiceInterface {
	Teacher findOne(Long id);
	List<Teacher> findAll();
	Teacher get(Long id) throws Exception;
	Teacher edit(Teacher teacher);
	Teacher insert(Teacher teacher);
	void delete(Long id);
	
}
