package com.example.EObrazovanje21.serviceInterface;

import java.util.List;

import com.example.EObrazovanje21.model.Account;

public interface AccountServiceInterface {

	Account findOne(Long id);

	List<Account> findAll();

	Account save(Account account);

	void remove(Long id);

	

}
