package com.example.EObrazovanje21.serviceInterface;

import java.util.List;

import com.example.EObrazovanje21.model.ExamPart;

public interface ExamPartServiceInterface {
	
	ExamPart findOne(Long id);

	List<ExamPart> findAll();

	ExamPart save(ExamPart examPart);

	void remove(Long id);

}
