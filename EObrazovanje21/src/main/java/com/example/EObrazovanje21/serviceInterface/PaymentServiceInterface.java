package com.example.EObrazovanje21.serviceInterface;

import java.util.List;

import com.example.EObrazovanje21.model.Payment;

public interface PaymentServiceInterface {
	
	Payment findOne(Long id);
	
	List<Payment> findAll();
	
	Payment save(Payment payment);
	
	void remove(Long id);

}
