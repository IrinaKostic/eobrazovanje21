package com.example.EObrazovanje21.serviceInterface;

import java.util.List;

import com.example.EObrazovanje21.model.DocumentType;

public interface DocumentTypeServiceInterface {
	
	DocumentType findOne(Long id);
	
	List<DocumentType> findAll();
	
	DocumentType save(DocumentType documentType);
	
	void remove(Long id);

}
