package com.example.EObrazovanje21.serviceInterface;

import java.util.List;

import com.example.EObrazovanje21.model.Document;



public interface DocumentServiceInterface {
	
	Document findOne(Long id);
	
	List<Document> findAll();
	
	Document save(Document document);
	
	void remove(Long id);

}
