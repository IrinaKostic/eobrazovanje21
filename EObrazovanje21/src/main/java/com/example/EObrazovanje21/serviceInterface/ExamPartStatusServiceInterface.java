package com.example.EObrazovanje21.serviceInterface;

import java.util.List;

import com.example.EObrazovanje21.model.ExamPartStatus;

public interface ExamPartStatusServiceInterface {
	
	ExamPartStatus findOne(Long id);

	List<ExamPartStatus> findAll();

	ExamPartStatus save(ExamPartStatus examPartStatus);

	void remove(Long id);

}
