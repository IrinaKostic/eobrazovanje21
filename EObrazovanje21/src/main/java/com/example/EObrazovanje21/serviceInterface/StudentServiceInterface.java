package com.example.EObrazovanje21.serviceInterface;

import java.util.List;

import com.example.EObrazovanje21.model.Student;


public interface StudentServiceInterface {
	
	Student findOne(Long id);
	
	List<Student> findAll();
	
	Student save(Student student);
	
	void remove(Long id);

	Student findByCard(String cardNumber);

	List<Student> findByUmnc(int umnc);


	

}
