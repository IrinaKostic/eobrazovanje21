package com.example.EObrazovanje21.serviceInterface;

import java.util.List;

import com.example.EObrazovanje21.model.ExamPartType;

public interface ExamPartTypeServiceInterface {
	
	ExamPartType findOne(Long id);

	List<ExamPartType> findAll();

	ExamPartType save(ExamPartType examPartType);

	void remove(Long id);
}
