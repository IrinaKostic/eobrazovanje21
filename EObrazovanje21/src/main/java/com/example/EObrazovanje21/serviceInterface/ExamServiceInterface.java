package com.example.EObrazovanje21.serviceInterface;

import java.util.List;

import com.example.EObrazovanje21.model.Exam;

public interface ExamServiceInterface {
	
	Exam findOne(Long id);

	List<Exam> findAll();

	Exam save(Exam exam);

	void remove(Long id);

}
