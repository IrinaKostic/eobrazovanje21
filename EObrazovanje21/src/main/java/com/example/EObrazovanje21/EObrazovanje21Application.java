package com.example.EObrazovanje21;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EObrazovanje21Application {

	public static void main(String[] args) {
		SpringApplication.run(EObrazovanje21Application.class, args);
	}

}
