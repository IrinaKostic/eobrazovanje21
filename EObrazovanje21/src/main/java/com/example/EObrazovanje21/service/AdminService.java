package com.example.EObrazovanje21.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired
;
import org.springframework.stereotype.Service;

import com.example.EObrazovanje21.model.Admin;
import com.example.EObrazovanje21.repository.AdminRepository;
import com.example.EObrazovanje21.serviceInterface.AdminServiceInterface;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;






@Service
public class AdminService implements AdminServiceInterface {
	
	@Autowired
	AdminRepository adminRepository;
	
	public Admin findOne(Long id) {
		return adminRepository.findById(id).orElse(null);
	}
	public List<Admin> findAll() {
		return adminRepository.findAll();
	}
	public Page<Admin> findAll(Pageable page) {
		return adminRepository.findAll(page);
	}
	public Admin save(Admin admin) {
		return adminRepository.save(admin);
	}

	public void remove(Long id) {
		adminRepository.deleteById(id);
	}

}
