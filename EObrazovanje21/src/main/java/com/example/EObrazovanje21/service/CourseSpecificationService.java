package com.example.EObrazovanje21.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.example.EObrazovanje21.model.CourseSpecification;
import com.example.EObrazovanje21.repository.CourseSpecificationRepository;
import com.example.EObrazovanje21.serviceInterface.CourseSpecificationServiceInterface;

@Service
public class CourseSpecificationService implements CourseSpecificationServiceInterface {
	
	@Autowired
	CourseSpecificationRepository courseSpecificationRepository;
	
	public CourseSpecification findOne(Long id) {
		return courseSpecificationRepository.findById(id).orElse(null);
	}
	public List<CourseSpecification> findAll() {
		return courseSpecificationRepository.findAll();
	}
	public Page<CourseSpecification> findAll(Pageable page) {
		return courseSpecificationRepository.findAll(page);
	}
	public CourseSpecification save(CourseSpecification courseSpecification) {
		return courseSpecificationRepository.save(courseSpecification);
	}

	public void remove(Long id) {
		courseSpecificationRepository.deleteById(id);
	}

}
