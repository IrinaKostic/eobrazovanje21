package com.example.EObrazovanje21.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.EObrazovanje21.model.DocumentType;
import com.example.EObrazovanje21.repository.DocumentTypeRepository;
import com.example.EObrazovanje21.serviceInterface.DocumentTypeServiceInterface;

@Service
public class DocumentTypeService implements DocumentTypeServiceInterface {

	@Autowired
	DocumentTypeRepository documentTypeRepository;
	
	@Override
	public DocumentType findOne(Long id) {
		return documentTypeRepository.findById(id).orElse(null);
	}

	@Override
	public List<DocumentType> findAll() {
		return documentTypeRepository.findAll();
	}

	@Override
	public DocumentType save(DocumentType documentType) {
		return documentTypeRepository.save(documentType);
	}

	@Override
	public void remove(Long id) {
		System.out.println("Removing in DocumentType service...");
		documentTypeRepository.deleteById(id);
	}
}
