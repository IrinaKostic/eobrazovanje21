package com.example.EObrazovanje21.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.EObrazovanje21.repository.ExamPartRepository;
import com.example.EObrazovanje21.serviceInterface.ExamPartServiceInterface;

import java.util.List;

import com.example.EObrazovanje21.model.ExamPart;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


@Service
public class ExamPartService implements ExamPartServiceInterface {
	
	@Autowired
	ExamPartRepository examPartRepository;

	@Override
	public ExamPart findOne(Long id) {
		return examPartRepository.findById(id).orElse(null);
	}

	@Override
	public List<ExamPart> findAll() {
		return examPartRepository.findAll();
	}

	@Override
	public ExamPart save(ExamPart examPart) {
		return examPartRepository.save(examPart);
	}

	@Override
	public void remove(Long id) {
		examPartRepository.deleteById(id);
	}
	

}

