package com.example.EObrazovanje21.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.EObrazovanje21.repository.ExamPartTypeRepository;
import com.example.EObrazovanje21.serviceInterface.ExamPartTypeServiceInterface;

import java.util.List;

import com.example.EObrazovanje21.model.ExamPartType;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

@Service
public class ExamPartTypeService implements ExamPartTypeServiceInterface  {
	
	@Autowired
	ExamPartTypeRepository examPartTypeRepository;
	
	@Override
	public ExamPartType findOne(Long id) {
		return examPartTypeRepository.findById(id).orElse(null);
	}

	@Override
	public List<ExamPartType> findAll() {
		return examPartTypeRepository.findAll();
	}

	@Override
	public ExamPartType save(ExamPartType examPartType) {
		return examPartTypeRepository.save(examPartType);
	}

	@Override
	public void remove(Long id) {
		examPartTypeRepository.deleteById(id);
	}
	

}

