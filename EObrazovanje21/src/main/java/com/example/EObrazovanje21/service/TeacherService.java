package com.example.EObrazovanje21.service;

import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.EObrazovanje21.model.Teacher;
import com.example.EObrazovanje21.repository.TeacherRepository;
import com.example.EObrazovanje21.serviceInterface.TeacherServiceInterface;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


@Service
public class TeacherService implements TeacherServiceInterface {
	
	@Autowired
	TeacherRepository teacherRepository;

	
	public Teacher findOne(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Teacher> findAll() {
		// TODO Auto-generated method stub
		return null;
	}
	public Page<Teacher> findAll(Pageable page) {
		return teacherRepository.findAll(page);
	}
	public Teacher save(Teacher teacher) {
		return teacherRepository.save(teacher);
	}

	@Override
	public Teacher get(Long id) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Teacher edit(Teacher teacher) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Teacher insert(Teacher teacher) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void delete(Long id) {
		teacherRepository.deleteById(id);
		
	}

	
}
