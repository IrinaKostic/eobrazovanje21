package com.example.EObrazovanje21.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.EObrazovanje21.model.Document;
import com.example.EObrazovanje21.repository.DocumentRepository;
import com.example.EObrazovanje21.serviceInterface.DocumentServiceInterface;

@Service
public class DocumentService implements DocumentServiceInterface {
	
	@Autowired
	DocumentRepository documentRepository;
	
	@Override
	public Document findOne(Long id) {
		return documentRepository.findById(id).orElse(null);
	}

	@Override
	public List<Document> findAll() {
		return documentRepository.findAll();
	}

	@Override
	public Document save(Document document) {
		return documentRepository.save(document);
	}

	@Override
	public void remove(Long id) {
		documentRepository.deleteById(id);
	}
	

}
