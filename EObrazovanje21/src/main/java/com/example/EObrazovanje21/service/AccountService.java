package com.example.EObrazovanje21.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.EObrazovanje21.model.Account;

import com.example.EObrazovanje21.repository.AccountRepository;
import com.example.EObrazovanje21.serviceInterface.AccountServiceInterface;



@Service
public class AccountService implements AccountServiceInterface {
	
	@Autowired
	AccountRepository accountRepository;
	
	@Override
	public Account findOne(Long id) {
		return accountRepository.findById(id).orElse(null);
	}

	@Override
	public List<Account> findAll() {
		return accountRepository.findAll();
	}

	@Override
	public Account save(Account account) {
		return accountRepository.save(account);
	}

	@Override
	public void remove(Long id) {
		accountRepository.deleteById(id);
	}

	
}
