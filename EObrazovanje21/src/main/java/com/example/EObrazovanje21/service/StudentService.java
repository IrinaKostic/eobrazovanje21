package com.example.EObrazovanje21.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.EObrazovanje21.model.Student;
import com.example.EObrazovanje21.repository.StudentRepository;
import com.example.EObrazovanje21.serviceInterface.StudentServiceInterface;



@Service
public class StudentService implements StudentServiceInterface {
	
	@Autowired
	StudentRepository studentRepository;

	@Override
	public Student findOne(Long id) {
		return studentRepository.findById(id).orElse(null);
	}

	@Override
	public List<Student> findAll() {
		return studentRepository.findAll();
	}

	@Override
	public Student save(Student student) {
		return studentRepository.save(student);
	}

	@Override
	public void remove(Long id) {
		studentRepository.deleteById(id);
	}
	
	@Override
	public Student findByCard(String cardNumber) {
		return studentRepository.findOneByCardNumber(cardNumber);
	}
	
	@Override
	public List<Student> findByUmnc(int umnc) {
		return studentRepository.findAllByUmnc(umnc);
	}

}
