package com.example.EObrazovanje21.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.EObrazovanje21.repository.ExamPartStatusRepository;
import com.example.EObrazovanje21.serviceInterface.ExamPartStatusServiceInterface;

import java.util.List;

import com.example.EObrazovanje21.model.ExamPartStatus;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


@Service
public class ExamPartStatusService implements ExamPartStatusServiceInterface {
	
	@Autowired
	ExamPartStatusRepository examPartStatusRepository;

	@Override
	public ExamPartStatus findOne(Long id) {
		return examPartStatusRepository.findById(id).orElse(null);
	}

	@Override
	public List<ExamPartStatus> findAll() {
		return examPartStatusRepository.findAll();
	}

	@Override
	public ExamPartStatus save(ExamPartStatus examPartStatus) {
		return examPartStatusRepository.save(examPartStatus);
	}

	@Override
	public void remove(Long id) {
		examPartStatusRepository.deleteById(id);
	}
	

}

