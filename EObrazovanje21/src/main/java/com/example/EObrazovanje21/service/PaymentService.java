package com.example.EObrazovanje21.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.EObrazovanje21.model.Payment;
import com.example.EObrazovanje21.repository.PaymentRepository;
import com.example.EObrazovanje21.serviceInterface.PaymentServiceInterface;

@Service
public class PaymentService implements PaymentServiceInterface {
	
	@Autowired
	PaymentRepository paymentRepository;

	@Override
	public Payment findOne(Long id) {
		return paymentRepository.findById(id).orElse(null);
	}

	@Override
	public List<Payment> findAll() {
		return paymentRepository.findAll();
	}

	@Override
	public Payment save(Payment payment) {
		return paymentRepository.save(payment);
	}

	@Override
	public void remove(Long id) {
		paymentRepository.deleteById(id);
	}

}
