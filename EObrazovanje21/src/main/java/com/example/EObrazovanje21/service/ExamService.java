package com.example.EObrazovanje21.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.EObrazovanje21.repository.ExamRepository;
import com.example.EObrazovanje21.serviceInterface.ExamServiceInterface;

import java.util.List;

import com.example.EObrazovanje21.model.Exam;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


@Service
public class ExamService implements ExamServiceInterface{

	@Autowired
	ExamRepository examRepository;
	
	@Override
	public Exam findOne(Long id) {
		return examRepository.findById(id).orElse(null);
	}

	@Override
	public List<Exam> findAll() {
		return examRepository.findAll();
	}

	@Override
	public Exam save(Exam exam) {
		return examRepository.save(exam);
	}

	@Override
	public void remove(Long id) {
		examRepository.deleteById(id);
	}
	

}

